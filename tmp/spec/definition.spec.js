'use strict';

var i = require('../../lib/interpreter.js');

describe('definision', function () {
  it('should def constants', function () {
    expect(function () {
      try {
        i.exec('(def x 10)');
      } catch (err) {
        console.log(err.message);
      }
    }).not.toThrow();
  });

  it('should def a list by using a backquote', function () {
    var result = i.exec('`("a" "b" "c")');
    expect(result).toEqual(["a", "b", "c"]);
  });

  it('should return a value', function () {
    try {
      var result = i.exec('(def x 10)\n(x)');

      expect(result).toBe(10);
    } catch (err) {
      console.log(err.message);
    }
  });

  it('should defind a string and then return it', function () {
    var result = i.exec('(def hello "hello world")(hello)');

    expect(result).toBe('hello world');
  });

  it('should exec more than one definition', function () {
    var result = i.exec('(def foo "hello world")\n\n(def bar 10)\n\n\n(bar)');
    expect(result).toBe(10);
  });

  it('should def a function with two params', function () {
    var result = i.exec('(def (sum a b) (+ a b))(sum 2 3)');

    expect(result).toBe(5);
  });

  it('should define a lambda with two or more args', function () {
    var result = i.exec('\n        (def test ($_ (a b) (+ a b)))\n        (test 10 20)\n        ');

    expect(result).toBe(30);
  });

  it('should return result of a last expression', function () {
    var result = i.exec('\n                          (let ((x 2) (y 3))\n                              (let ((foo ($_ (z) (+ x y z)))\n                                    (x 7))\n                                (foo 4)))\n                            ');

    expect(result).toBe(14);
  });

  describe('set', function () {
    it('should update a value in a variable', function () {
      var result = i.exec('(def a 10)\n(set! a 20)\n(a)');

      expect(result).toBe(20);
    });

    it('should ignore spasec', function () {
      var result1 = i.exec('(def a 10)(set! a 20)(a)');
      var result2 = i.exec('(def a 10) (set! a 20)(a)');
      var result3 = i.exec('(def a 10) (set! a 20) (a)');
      var result4 = i.exec('(def a 10)(set! a 20) (a)');

      expect(result1).toBe(20);
      expect(result2).toBe(20);
      expect(result3).toBe(20);
      expect(result4).toBe(20);
    });
  });
});